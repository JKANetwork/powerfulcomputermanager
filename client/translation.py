import configparser

t_cp = configparser.ConfigParser()
t_cp.read('configpcm.ini')

t_tlang = t_cp['General']['lang']

def T(textid):
	textid = textid.strip()
	global t_tlang
	try:
		if t_tlang == 'es':
			return T_es(textid)
		elif t_tlang == 'en':
			return T_en(textid)
		else:
			return textid
	except:
		return textid
	


def T_es(textid):
	dict = {
    'Main menu': 'Menú principal',
    'Select option': 'Seleccione opción',
    'Computers': 'Equipos',
    'Groups': 'Grupos',
	'Cooks': 'Recetas',
	'Computers list': 'Lista de equipos',
	'Groups list': 'Lista de grupos',
	'Computer deleted': 'Equipo eliminado',
	'Add computer': 'Añadir equipo',
	'Remove computer': 'Borrar equipo',
	'Add group': 'Añadir grupo',
	'Remove group': 'Borrar grupo',
	'Group deleted': 'Grupo eliminado',
	'Select a computer': 'Selecciona un equipo',
	'Computer status': 'Estado de un equipo',
	'Add computer to group':'Añadir equipo a un grupo',
	'Add cook to a group':'Añadir receta a un grupo',
	'Remove computer of a group':'Quitar equipo de un grupo',
	'Remove cook from a group':'Quitar receta de un grupo',
	'Name of the cook':'Nombre de la receta',
	'Select a group': 'Seleccione un grupo',
	'Cooks of computer': 'Recetas del equipo',
	'Groups of computer': 'Grupos del equipo',
	'Cooks pending of computer': 'Recetas pendientes del equipo',
	'New group name': 'Nombre del nuevo grupo',
	'Computers menu': 'Menú equipos',
	'Groups menu': 'Menú grupos',
	'Cooks menu': 'Menú recetas',
	'Reports menu': 'Menú reportes',
	'List of computers and cooks of a group': 'Listado de equipos y recetas de un grupo',
	'List of groups of a cook': 'Listado de grupos de una receta',
	'Select computer to add to a group': 'Seleccione equipo para añadir a un grupo',
	'Select group to be added': 'Seleccione grupo a ser añadido',
	'Select group to delete': 'Seleccione grupo a quitar',
	'Rename cook': 'Renombrar receta',
	'Set password': 'Establecer contraseña',
	'There is no password yet in PCM. Please set one below':'No hay contraseña aún en PCM. Por favor, escriba una debajo',
	'Confirm it': 'Confirmalo',
	'Password not match. App will exit': 'Las contraseñas no coinciden. La aplicación se va a cerrar',
	'Enter password': 'Introducir contraseña',
	'Enter password to start': 'Introduzca la contraseña para empezar',
	'Clean cook status': 'Borrar estado de una receta',
	'With errors': 'Con errores',
	'Error talking with API': 'Error en la comunicación con la API',
	'Cook details':'Detalles de una receta',
	'Cook groups':'Grupos de una receta'
    }
	
	return dict[textid]
	
def T_en(textid):
	return textid #English doesn't have to be translated