#!/usr/bin/python3
from flask import Flask, url_for, render_template, request, Response,redirect,make_response
import os
import jinja2
import requests
import hashlib #SHA256
import json
import base64
import configparser
from datetime import datetime
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

BUILD=2
APIC_VER=2

# Load config to comunicate with API or use default
try:
	config = configparser.ConfigParser()
	config.read(os.path.relpath('..\\client\\configpcm.ini'))
	c_server = config['General']['server']
except:
	c_server = "https://127.0.0.1:3333" #Default server api address

app = Flask(__name__,static_url_path="/assets", static_folder='assets', template_folder="templates")


def returnvalueapi(suburl,field="Name"):
	global c_server
	passha256=request.cookies.get('admin_logued')
	if 'Password=' in suburl:
		twopart = ""
	elif '?' in suburl:
		twopart = '&Password='+str(passha256)
	else:
		twopart = '?Password='+str(passha256)
	
	r=(requests.post(c_server+suburl+twopart, verify=False).text).replace("\'", "\"").replace('\\"',"'").replace(': None',': "None"')
	try:
	#print(suburl+twopart+"->"+r)
		jsonobj = json.loads(r)
		if isinstance(jsonobj, list):
			jsonobj = jsonobj[0]
		return jsonobj[field]
	except:
	  	print('Error: '+str(r))
	  	return str({'TEXT':r,'RESULT':'ERROR'})
		
def returnTable(suburl,field=["Name"]):
	global c_server
	passha256=request.cookies.get('admin_logued')
	if isinstance(field,str):
		if field != 'ASIS': # Do not touch if it says that we have to return table AS IS
			field=[field] # Convert to list
	if 'Password=' in suburl:
		twopart = ""
	elif '?' in suburl:
		twopart = '&Password='+str(passha256)
	else:
		twopart= '?Password='+str(passha256)
	try:
		r=(requests.post(c_server+suburl+twopart, verify=False).text).replace("\'", "\"").replace('\\"',"'").replace(': None',': "None"')
		jsonobj = json.loads(r)
		if field == 'ASIS':
			return jsonobj
		else:
			listitems = []
			for ite in jsonobj: # Run in array from json
				to = []
				for i in field:
					to.append(str(ite[i]))
				listitems.append(to)
			return listitems
	except:
		return None


@app.route("/admin",methods=['GET'])
@app.route("/admin/",methods=['GET'])
def pAdminIndex():  #Admin Index page
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")
	table = returnTable("/get/computers",['ID_C','Name','RAM','CPUName','SOVersion','SOCaption','HDD','LastConnection','RAMFree'])

	for lista in table:
		lista.append(returnTable("/get/groups?ComputerID="+lista[0],["ID_G","Name"])) #lista[9]¿
		if lista[6] is not None:
			try:
				lista[6]=json.loads(base64.b64decode(lista[6]).decode('UTF-8'))
			except:
				lista[6]=json.loads(str("{}"))
		else:
			lista[6]=json.loads(str("{}"))
		#print (lista[6])
		try:
			lista[7] = datetime.fromtimestamp(int(lista[7])).strftime('%Y-%m-%d %H:%M:%S') # LastConnection
		except:
			lista[7] = "Never"
	return render_template('/adminindex.tmpl', title="Admin Dashboard",tablecomputers=table)

@app.route("/admin/addcomputer",methods=['GET'])
def pAdminAddcomputer():
	computeradd=request.args.get('computeradd')
	if computeradd is not None:
		r = returnvalueapi("/add/computer?ComputerName="+computeradd,field="RESULT")
		if r == "OK":
			return redirect('/admin')
		else:
			return "ERROR"
	else:
			return redirect('/admin')

@app.route("/admin/delcomputer",methods=['GET'])
def pAdminDelcomputer():
	computerdel=request.args.get('computerdel')
	r = returnvalueapi("/del/computer?ComputerID="+computerdel,field="RESULT")
	if r == "OK":
		return redirect('/admin')
	else:
		return "ERROR"

@app.route("/admin/computer",methods=['GET'])
def pAdminComputer():  #Admin see one group
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")

	computerid=request.args.get('ID_C')
	if computerid is not None and computerid.isnumeric():
		computername = returnvalueapi('/get/computers?ComputerID='+str(computerid),"Name")
		groups = returnTable("/get/groups?ComputerID="+str(computerid),['ID_G','Name'])
		for key in range(len(groups)): #Cookgs of every group
			groups[key].append(returnTable("/get/cookgrp?GroupID="+groups[key][0],['CookName']))

		allgroups = returnTable("/get/groups",['ID_G','Name']) #All groups, for adding one
		return render_template('/admincomputer.tmpl', title="Computer "+computername,computerid=computerid,computername=computername,groups=groups,allgroups=allgroups)
	return redirect('/admin')


@app.route("/admin/computer/addgroup",methods=['GET'])
def pAdminComputerAddGroup():
	computerid=request.args.get('computeridadd')
	groupid=request.args.get('groupidadd')
	if computerid is not None and groupid is not None:
		r = returnvalueapi("/add/grpcomputer?ComputerID="+computerid+"&GroupID="+str(groupid),field="RESULT")
		if r == "OK":
			return redirect('/admin/computer?ID_C='+computerid)
		else:
			return "ERROR"
	else:
			return redirect('/admin')

@app.route("/admin/computer/delgroup",methods=['GET'])
def pAdminComputerDelGroup():
	computerid=request.args.get('computeriddel')
	groupname=request.args.get('groupnamedel')
	if computerid is not None and groupname is not None:
		r = returnvalueapi("/del/grpcomputer?ComputerID="+computerid+"&GroupName="+str(groupname),field="RESULT")
		if r == "OK":
			return redirect('/admin/computer?ID_C='+computerid)
		else:
			return "ERROR"
	else:
			return redirect('/admin')


@app.route("/admin/groups",methods=['GET'])
@app.route("/admin/groups/",methods=['GET'])
def pAdminGroups():  #Admin Index groups
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")
	table = returnTable("/get/groups",['ID_G','Name'])

	for lista in table:
		lista.append(returnTable("/get/computersgrp?GroupID="+lista[0],['ID_C','Name'])) #lista[3]¿
		lista.append(returnTable("/get/cookgrp?GroupID="+lista[0],['CookName'])) #lista[4]¿
	return render_template('/admingroups.tmpl', title="Groups",tablegroups=table)


@app.route("/admin/group",methods=['GET'])
def pAdminGroup():  #Admin see one group
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")

	groupid=request.args.get('ID_G')
	groupname=request.args.get('GroupName')

	if groupid is None and groupname is not None:
		groupid = returnvalueapi('/get/groups?GroupName='+str(groupid),"ID_G")

	if groupid is not None and str(groupid).isnumeric():
		groupname=returnvalueapi('/get/groups?GroupID='+str(groupid),"Name")
		computers = returnTable("/get/computers",['ID_C','Name'])
		datagroup = (returnTable("/get/computersgrp?GroupID="+groupid,['ID_C','Name'])) #lista[0]¿
		tem = (returnTable("/get/cookgrp?GroupID="+groupid,['CookName'])) #lista[1]¿
		cooksdata = []
		for cook in tem:
			to = []
			to.append(cook[0])
			to.append(int(returnvalueapi('/get/lastrevisioncook?CookName='+cook[0],"Revision"))) # Has to be a number
			tabletemp = returnTable("/get/statuscook?CookName="+cook[0],'ASIS')
			print (tabletemp)
			for x in range(len(tabletemp)):
				print(type(tabletemp[x]['Revision']))
				if type(tabletemp[x]['Revision']) != int: # Si no es un número, cambiar.
					tabletemp[x]['Revision'] = 0
			to.append(tabletemp)
			cooksdata.append(to)

		tem = (returnTable("/get/cookall",'ASIS')) #Lista de todas las recetas
		allcooks = []
		for cook in tem['CookName']:
			to = []
			to.append(cook)
			to.append(returnvalueapi('/get/lastrevisioncook?CookName='+cook,"Revision"))
			allcooks.append(to)
		
		return render_template('/admingroup.tmpl', title="Group "+groupname,groupname=groupname,groupid=groupid,datagroup=datagroup,cooksdata=cooksdata,computers=computers,allcooks=allcooks)
	return redirect('/admin/groups')

@app.route("/admin/group/addcook",methods=['GET'])
def pAdminGroupAddcook(): #Add cook form enter
	cooktoadd=request.args.get('cooknameadd')
	groupid=request.args.get('groupid')
	if cooktoadd != None and groupid != None:
		r = returnvalueapi("/add/cookgrp?CookName="+cooktoadd+"&GroupID="+groupid,field="RESULT")
		if r == "OK":
			return redirect('/admin/group?ID_G='+groupid)


@app.route("/admin/group/addgroup",methods=['GET'])
def pAdminGroupAddgroup(): #Add cook form enter
	groupname=request.args.get('groupnameadd')
	if groupname != None and groupname != "":
		r = returnvalueapi("/add/group?GroupName="+groupname,field="RESULT")
		if r == "OK":
			return redirect('/admin/group?GroupName='+groupname)


@app.route("/admin/group/addcomputer",methods=['GET'])
def pAdminGroupAddcomputer(): #Add computer form enter
	computertoadd=request.args.get('computeridadd')
	groupid=request.args.get('groupid')
	if computertoadd != None and groupid != None:
		r = returnvalueapi("/add/grpcomputer?ComputerID="+computertoadd+"&GroupID="+groupid,field="RESULT")
		if r == "OK":
			return redirect('/admin/group?ID_G='+groupid)


@app.route("/admin/group/delcomputer",methods=['GET'])
def pAdminGroupDelcomputer(): #Add computer form enter
	computertodel=request.args.get('computernamedel')
	groupid=request.args.get('groupid')
	if computertodel != None and groupid != None:
		r = returnvalueapi("/del/grpcomputer?ComputerName="+computertodel+"&GroupID="+groupid,field="RESULT")
		if r == "OK":
			return redirect('/admin/group?ID_G='+groupid)
	else:
		return redirect('/admin/group?ID_G='+groupid)


@app.route("/admin/cook",methods=['GET'])
def pAdminCook():  #Admin Index groups
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")

	cookname=request.args.get('CookName')
	if cookname != None:
		groupscook=returnTable('/get/grpcook?CookName='+str(cookname),['Name'])
		allgroups=returnTable('/get/groups',['ID_G','Name'])
		return render_template('/admincook.tmpl', title="Cook "+cookname,cookname=cookname,allgroups=allgroups,groupscook=groupscook)

@app.route("/admin/cook/addgroup",methods=['GET'])
def pAdminCookAddgroup(): # Add cook to group
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")
	cookname=request.args.get('cooknameadd')
	groupid=request.args.get('groupidadd')
	if cookname != None and groupid != None:
		returnvalueapi('/add/cookgrp?CookName='+cookname+'&GroupID='+groupid,'RESULT')
		return redirect('/admin/cook?CookName='+cookname)
	else:
		return "Error of arguments"
		
@app.route("/admin/cook/delgroup",methods=['GET'])
def pAdminCookDelgroup(): # Add cook to group
	if returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE') != '0':
		return render_template('/login.tmpl', title="Login")
	cookname=request.args.get('cooknamedel')
	groupname=request.args.get('groupnamedel')
	if cookname != None and groupname != None:
		ret = returnvalueapi('/del/cookgrp?CookName='+cookname+'&GroupName='+groupname,'RESULT')
		if ret != '0':
			return "Error "+str(ret)
		return redirect('/admin/cook?CookName='+cookname)
	else:
		return "Error of arguments"

#First time password set
@app.route("/firsttime",methods=['POST'])
def pFirstTime(): #Add computer form enter
	passwd = request.args.get('password')
	passwd = hashlib.sha256(passwd.encode()).hexdigest()
	if passwd != None:
		r = returnvalueapi("/upd/password?NewPassword="+passwd,field="RESULT")
		if r == "OK":
			res = make_response(redirect(url_for('pAdminIndex')))
			res.set_cookie("admin_logued",value=passwd)
		else: #Hmm..Unespecified error 
			render_template('/firsttime.tmpl')
	else:
		return render_template('/firsttime.tmpl')

@app.route("/",methods=['GET'])
def pIndex():  #Index page
	howispasswd=returnvalueapi('/check/password?Password='+str(request.cookies.get('admin_logued')),'EXITCODE')
	if howispasswd == '0': # Logued
		return redirect(url_for('pAdminIndex'))
	elif howispasswd == '1' or howispasswd == '2': # Has to login
		return render_template('/login.tmpl')
	elif howispasswd == '3': # No password yet
		return render_template('/firsttime.tmpl')

@app.route("/login",methods=['GET','POST'])
def pLogin():  #Login
	if request.values.get('password'):
		trpass = hashlib.sha256(request.values.get('password').encode()).hexdigest()
		howispasswd = returnvalueapi('/check/password?Password='+trpass,'EXITCODE')
		if howispasswd == '0':
			res = make_response(redirect(url_for('pAdminIndex')))
			res.set_cookie("admin_logued",value=trpass)
			return res
		elif howispasswd in (1,2):
			return render_template('/login.tmpl')
		elif howispasswd == '3': # No password yet
			return render_template('/firsttime.tmpl')
	else:
		return render_template('/login.tmpl')

print("Build: "+str(BUILD))
print("API Client Compatible Version: "+str(APIC_VER))

app.run(debug=True,port=3434,ssl_context='adhoc',host='0.0.0.0',threaded=True) #Default is port 3434