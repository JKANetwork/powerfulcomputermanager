# Build 3
import sqlite3
from shutil import copyfile
from os import path
from datetime import datetime

if not path.isfile('BD/database.db'):
	copyfile('BD/emptydatabase.db','BD/database.db')

def logit(text):
	text = str(text)
	now = datetime.now()
	print ("Log: " + text)
	with open('api.log', 'a') as file:
		file.write("Log ("+now.strftime("%x %X") + "): ")
		file.write(text)
		file.write('\n')

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
 

def select(query):
	conn = sqlite3.connect('BD/database.db')
	conn.row_factory = dict_factory
	if debugsql==True:
		logit("SQL: "+query)
	cur = conn.execute(query)
	return cur.fetchall()

def insert(query):
	conn = sqlite3.connect('BD/database.db')
	if debugsql==True:
		logit("SQL: "+query)
	try:
		c = conn.cursor()
		c.execute(query)
		conn.commit()
		conn.close()
		if debugsql==True:
			logit("SQL Ok")
		return str({'RESULT': 'OK'})
	except:
		if debugsql==True:
			logit("SQL Error")
		return {'RESULT': 'SQLite3 Error'}

def retOption(option):
	try:
		return select("SELECT Value FROM OPTIONS WHERE Option='"+option+"'")[0]['Value']
	except:
		return None
		
debugsql = retOption('DebugSQL')

#Make updates if neccesary
sqlv=retOption('SQLVersion')
if sqlv == 1:
	pass #Its latest version of SQL