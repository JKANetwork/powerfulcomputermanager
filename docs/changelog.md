# Changelog

## 2021/12/15
- Some fixes in code
- NuGet try to avoid stuck on a question

## 2020/09/11
- Fixes in webui in some options
- Fix IF64BIT/IF32BIT, was not working
- Some other fixes

## 2020/07/16
- Fixes and improvments to web gui
- General bugfix

## 2020/07/03
- Fixes to web gui and some new options

## 2020/03/17
- New function copy
- Some fixes

## 2020/02/20
- Fixing more bugs
- First public beta of web gui, it doesn't have some parts but it's working

## 2019/11/21
- Fixing bugs of last change and cleaning some code
- Preparing web gui (It's hide yet)

## 2019/11/20
- New API (2). All requests now works in POST and SSL (Self-Signed)
- Now using Flask and not falcon with waitress-serve (Lesser dependencies and ssl support)
- Some new options
- More stable (Besides this API is at beta stage because POST and different json returns)
- Better doc
- Preparing for better gui (Web one)
- Now SQL will have versions, for updating if neccesary (If you are using old version, please create in database new table "OPTIONS" with data that has "emptydatabase.db")