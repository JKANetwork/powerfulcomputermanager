# What is "Powerful Computer Manager"?
Powerful Computer Manager is the super-simple-but-powerful program to automatizate tasks in groups of computers (In a Active Directory site, or not) in a unattended way.
It inteed to be a simple [SCCM](https://en.wikipedia.org/wiki/Microsoft_System_Center_Configuration_Manager) alternative, that inventarizes computers and install/remove software in it, tweak system of run arbitrary commands.

You only have to write simple _cooks_ that are small yaml text files, create groups of computers and assign _cooks_ to this groups, and will be run in the destination computers


# Disclaimer

I'm using this program in the company I work with 12 computers with Windows 8.1/10 with various software (MSI,EXE,...) and tweaking services, and it works right

It's not error proof, then, I don't have any responsability if something crashes, but you can open a Issue and will see it

# Roadmap

- Complete translation English and Spanish
- Stabilize all
- Support Linux clients
- Have WebUI completed
  - Todo: Create Groups

# Requirements

## Server
For server you can use Linux, Mac or Windows, and in theory any environment that can run Python 3 can work as server

- Python3 with:
	- Flask
	- pyopenssl
	- Yaml
	- Python3 deps can be installed it using pip install command (pip install pyopenssl flask pyyaml)

- For controlling the program, you have to enter in a browser and go to https://SERVERIP:3434 (By default)
	- At first time yet, for putting password, you have to start control.pyw using Python3 (Needs easygui, pip install easygui). It doesn't need to be started in server, but it has to be in that folder (Or a folder with configpcm.ini file).

## Clients
-	Windows:
	-	Windows 7 SP1 absolute minimal, but its only officially tested in 8.1 and 10, Windows 7 is obsolete by Microsoft.
	-	It needs Powershell 5.0+ (Or Powershell Core 6.2+), lower versions doesn't have some of the commands and are not guaranteed to work, not tested. In Windows 7 and 8.1, you have to update Powershell. Windows 10 have Powershell 5 by default
		You can update from here:
			-Powershell Core: https://github.com/PowerShell/PowerShell/releases
			-Powershell 5.1: https://www.microsoft.com/en-us/download/details.aspx?id=54616 )

-	Linux:
	-	Not supported yet, but when Windows be stable, I will try to do using bash scripts
	
- Clients may have to be queried by DNS requests. If you don't have a domain (Samba or Windows AD), you may have to have a DNS server with local machines

# Instalation 

## Server
-   Copy this folder to a folder in your server
-	Install dependencies
-	Start the server: Start api.py using loadserver.bat (Windows) o loadserver.sh (Linux) file. You can do in systemd way, or a cron, or in a terminal of windows server,... It also will start the webserver gui app for controlling it (https://localhost:3434).

## Client. Example using GPO and Task Schedule
-	Go to "client" folder and copy "configpcm_example.ini" to "configpcm.ini" and change their values to yours. Change lang value for having client gui translated to your lang. Here you will see:
	- server=X -> Its where server app is running and port by default is :3333. You can change in loadserver file. You can use an IP like 192.168.1.200:3333.
	- resources=\\XX -> UNC/Path where software are. When you write cooks, you will do come "REPOTOLOCAL|somefile.msi", well, this folder is where it will copy files. It has be readable by clients.
-   For clients to work, they have to run "client\client.ps1" every 30 mins/1 hour or so, I did using Active Directory and a Task Scheduled GPO (Copying client.ps1 and configpcm.ini files from Client to SysVol\DomainName\Scripts\)
    - User: SYSTEM    (Or NT\SYSTEM)
    - Command: powershell.exe   (Or pwsh.exe if using Powershell Core)
    - Args: -executionpolicy bypass -windowstyle hidden -noninteractive -nologo -file "\\SERVER\SysVol\DOMAINNAME\scripts\client.ps1"
	
	Note: client.ps1 has to have controlpcm.ini file with it and has to be readable and executable by clients
-	If you don't have Active Directory, you can do the Task Scheduled task in every computer without problems, and using any Shared folder, it will work right.

-   For use of param "-startup 1" for cooks that has to be run without users, you will have to create a Task Scheduled GPO when workstation startsup/powers off with param -startup 1, like:
	- Args: -executionpolicy bypass -windowstyle hidden -noninteractive -nologo -file "\\SERVER\SysVol\DOMAINNAME\scripts\client.ps1" -startup 1

-   Note: You can add Windows GPO to show the poweron/poweroff/logon/logoff scripts, I think can be great (Mostly if command stucks):
	- https://admx.help/?Category=Windows_10_2016&Policy=Microsoft.Policies.Scripts::Run_Startup_Script_Visible
	- https://admx.help/?Category=Windows_10_2016&Policy=Microsoft.Policies.Scripts::Run_Shutdown_Script_Visible
	- https://admx.help/?Category=Windows_10_2016&Policy=Microsoft.Policies.Scripts::Run_Logoff_Script_Visible
	- https://admx.help/?Category=Windows_10_2016&Policy=Microsoft.Policies.Scripts::Run_Logon_Script_Visible
	- (This in some enterprises is useful) https://admx.help/?Category=Windows_10_2016&Policy=Microsoft.Policies.WindowsLogon::VerboseStatus

## Control app
-	Start control.pyw to setup password
-   Go to a browser and load https://SERVER_IP:3434/ , login and manage all. control.pyw is the old way, it has to work, but its deprecated

-   For creating cooks, you can see the documentation (It takes 3 minutes to make something workable), and can use docs/example_cooks for examples. If anyone wants, I will setup a git repo for cooks.