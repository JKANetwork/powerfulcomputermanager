# File hierarchy

## Root folder
    - api.py -> Server file. It manages all requests from clients and connects to database. Runs at port 3333 by default
    - loadserver.bat -> A helper to run api.py and admin.py on Windows
    - loadserver.sh -> A helper to run api.py and admin.py on Linux
    - sql.py -> Helper for api.py sql sentences

## Client folder. Programs of client
    - client.ps1 -> This file has to run every X time (I suggest every hour) on every client and as NT\SYSTEM or local admin. You can do this with a Task Schedule as SYSTEM in a GPO (Active Directory)
    - control.pyw -> This is the gui of the program. With that you manages the client-server program. It has to be in same folder as configpcm.ini file
    - configpcm.ini -> Configuration for client and control.
	
## Admin folder. This is the folder for webapp that runs with server if using loadserver from root
	-admin.py -> Web app (By default, runs at port 3434)

## BD folder
    - database.db -> Database file in sqlite3

## cooks folder
    It has the cooks in yaml format. 
    Warning: Do not change name of cooks created and assigned directly. Use control for that

## Doc folder
    Documentation...

## REPO folder (This isn't needeed to be there)
    You have to have this shared on LAN, this is the folder that cooks use to retrieve your files (As .exe/.msi) to use in scripts.
