# Quick Start instructions

For be able to use this, after reading the installation section:

You have to create a cook. In "Examples" you have Cook Examples, copy one to cooks and edit (See howto_writecook)

Then, you have to create a group using the control.pyw GUI.

Computers can be into "Groups", and "Groups" can have "Cooks"

Cooks -> Group

Computers -> Group

You can have more than 1 cook in a group

Maybe in a simple organization, you can assign cook to group with same name, and computers to groups that need this cook