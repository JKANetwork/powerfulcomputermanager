# How to
Cooks are in yaml format, and have this structure.

First: Name, a line like below, with the name
"name: My Super Cook"
Then, the revision number, when this number changes, cook will be reapplied
"revision: 1"

If the cook has to run everytime client is called (And not only one time per new revision), you have to add:
"runever: 1" (1-> Everytime, if you change to 0 or delete, it will not do it)

If the cook has to run when no user is logued in, add:
"atstartup: 1"
Be aware that for be able to use this, in task scheduler (Or your favourite method) you have to run client.ps1 with argument "-startup 1" when no users are logued
"only64bit: 1"/"only32bit: 1" only for 64 or 32 bit systems (Maybe for some updates can be used)

And then, steps. First a "steps:" line, and then line by line the steps of the cook
"steps:
    - PWCMD|White-Output "Hello World"
"
Steps are ever put like "Type of step" | "Command"
You can put comments in every line putting another "|" and comments at finish, like.. "CMD|WinSCP-5.13.4-Setup.exe /VERYSILENT /NORESTART|Note: Installing WinSCP"

Cook finishes executing if they had and error (Except inside a NOERROR block), or if finishes right.

You have examples in the example_cooks folder

Note: Cooks start in directory $env:temp (%TEMP%) from SYSTEM user.

# Commands avaiable (LINUX IS NOT SUPPORTED YET)

| OS  	| COMMAND     	| Description                                                                             		         	|
|-----	|-------------	|---------------------------------------------------------------------------------------------------------	|
| W   	| CMD         	| Cmd command. Beware of quotes, because internally is run as cmd /c ""                        		    	|
| W/L 	| PWCMD       	| Powershell command, runs using Invoke-Command                                               		    	|
| W/L 	| REPOTOLOCAL 	| Copy a file from REPO folder configured to local in temp folder. You can copy a file or a folder. Do not put between "" 			|
| W/L	| COPY			| Copy a file from copied to local (REPOTOLOCAL) to any destiantion. Ex: COPY|filec.cfg;C:\Program\filec.cfg|
| W 	| INSTALLMSI 	| Installs an MSI file silently (Params /quiet /norestart are put, do not repeat). You can add parameters (Like INSTALLMSI|superapp.msi ADDLOCAL=extension SETASDEFAULT=1) 	|
| W/L 	| NOERROR	 	| Creates a block of instructions that not captures errors and continues event if fails<br>Example:<br>NOERROR|Try to delete a file that maybe can not exist<br>PWCMD|Remove-Item C:\Windows\loginstall.txt<br>ENDNOERROR|Finish |
| W/L 	| ENDNOERROR 	| Finishes NOERROR Block																				 	|
| W/L 	| SERV_ENABLE 	| Enables a service																						 	|
| W/L 	| SERV_DISABLE 	| Disables a service																					 	|
| W/L 	| MSG	 		| Shows a message to user																				 	|
| W/L 	| REM / COMMENT | Annotation for developers in cooks																	 	|
| W/L 	| KILL_PROCESS  | Kills a process																				 			|
| W/L 	| UNINSTALL		| Uninstalls a software by name. (It finishes using * at Windows 'Get-Package -Name "Program*"', be care)	|
| W/L 	| REMOVE		| Removes a file or folder path from local compuer. It's recursive											|
| W/L 	| SLEEP / PAUSE | Pauses execution seconds specified in param (SLEEP|3)														|
| W/L 	| DOWNLOAD *	| Downloads a file from network to tempFolder/filename. It's required to put a filename like: DOWNLOAD|http://example.org/file.zip;filename.zip. ";" separates URL and filename |
| W/L 	| EXIT			| Terminates cook execution. It can take a param as error text (EXIT|Error at removing file)				|
| L   	| BASH  	   	| Bash command                                                                         			           	|


- *DOWNLOAD: It seems to be a limit in PowerShell Invoke-WebRequest that limits this downloads to some like 128MB

Inside of a Cook, you can use a simple IF query:

| OS  	| COMMAND     		| Runs if when:                                                       		     	|
|-----	|-----------------	|---------------------------------------------------------------------------------	|
| W/L  	| IFSOFTWAREINST  	| When program is installed.		                        				    	|
| W/L 	| IFSOFTWAREVER	  	| When program is installed and has X version (Name;Version)    		            |
| W/L 	| IFPATHEXISTS	  	| When if file exists																|
| W/L 	| IFPWCMD		  	| When powershell command returns True (Beware of command executed if using Linux)	|
| W/L 	| IF64BIT		  	| When computer is running a 64 Bit operating system								|
| W/L 	| IF32BIT		  	| When computer is running a 32 Bit operating system								|
| W/L 	| IFWINDOWS		  	| When computer is running Windows 													|
| W/L 	| IFCOMPUTERNAME	| When computer name is Name 													|
| W/L 	| IFLINUX		  	| When computer is running Linux 													|
| L 	| IFBASH		  	| When bash command returns True 													|
| W/L 	| ELSE			  	| Else of IF...					 													|
| W/L 	| ENDIF			  	| Finishes IF block				 													|


